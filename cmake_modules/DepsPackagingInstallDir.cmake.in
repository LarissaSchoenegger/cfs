# create a zip for cfsdeps cache precompiled by install_manifest.txt

# the .cmake.in file is converted to .cmake  by DendencyTools.cmake with 
# generate_packing_scripts_manifest() by the use of configure_file()
# 
# configure_file() replaces @*@ variables. The resulting .cmake is called
# as post-install step of a cmake external project via cmake -P ...cmake,
# hence there is no context of the cmake variables and we need to "import" all via @*@.

# for the asserts
include("@CFS_SOURCE_DIR@/cmake_modules/CFS_macros.cmake")
include(CMakePrintHelpers)

assert_not_empty(@CFS_DEPS_CACHE_DIR@)
assert_not_empty(@PRECOMPILED_PCKG_FILE@)
assert_not_empty(@DEPS_INSTALL@)
assert_not_empty(@DEPS_LIB_TYPE@)

set(CMAKE_BINARY_DIR @CMAKE_BINARY_DIR@)
set(CFS_DEPS_CACHE_DIR @CFS_DEPS_CACHE_DIR@)
# e.g. /Users/fwein/code/cfsdeps_refactor/precompiled/arpack_3.7.0_MACOSX_12.6_ARM64_F-GNU-11.2.0.zip
set(PRECOMPILED_PCKG_FILE @PRECOMPILED_PCKG_FILE@)
# e.g. /Users/fwein/code/master/refactor/cfsdeps/arpack
set(DEPS_INSTALL @DEPS_INSTALL@)
set(DEPS_LIB_TYPE @DEPS_LIB_TYPE@) # "static", "dynamic" or "static-dynamic" # for WIN32 dynamic is in bin

if(NOT IS_DIRECTORY "${CFS_DEPS_CACHE_DIR}/precompiled}")
  file(MAKE_DIRECTORY "${CFS_DEPS_CACHE_DIR}/precompiled")
endif()

#cmake_print_variables(CFS_DEPS_CACHE_DIR)
#cmake_print_variables(PRECOMPILED_PCKG_FILE)
#cmake_print_variables(DEPS_INSTALL)

# standard make or configure does not necessarily know about lib
if(NOT IS_DIRECTORY "${DEPS_INSTALL}/lib")
  FILE(MAKE_DIRECTORY "${DEPS_INSTALL}/lib")
endif()

# copy any posssible lib64 to lib. 
if(EXISTS "${DEPS_INSTALL}/lib64")
  message("copy from ${DEPS_INSTALL}/lib64 to ${DEPS_INSTALL}/lib")
  file(COPY "${DEPS_INSTALL}/lib64/" DESTINATION "${DEPS_INSTALL}/lib")
endif()

# compress the directory but take only inlcude and lib/*.a or lib/*.lib and/or dynamic libs
if(DEPS_LIB_TYPE MATCHES "static")
  set(_LIBS_S ${DEPS_INSTALL}/lib/*${CMAKE_STATIC_LIBRARY_SUFFIX})
endif()
if(DEPS_LIB_TYPE MATCHES "dynamic")
  if(WIN32)
    set(_LIBS_D ${DEPS_INSTALL}/bin/*${CMAKE_DYNAMIC_LIBRARY_SUFFIX})
  else()  
    set(_LIBS_D ${DEPS_INSTALL}/lib/*${CMAKE_DYNAMIC_LIBRARY_SUFFIX})
  endif()  
endif()
  
# somehow the cmake tar command fails for directories when executed by a script but works on 
# command line. Hence work with a files list
file(GLOB_RECURSE _FILES_TO_ZIP "${DEPS_INSTALL}/include/*" "${_LIBS_S}" "${_LIBS_D}")
# message(STATUS "found ${_FILES_TO_ZIP}")
string(REPLACE ";" "\n" _FILES_TO_ZIP_N "${_FILES_TO_ZIP}")
# message(STATUS "packing ${_FILES_TO_ZIP_N}")
file(WRITE "${DEPS_INSTALL}/files_to_zip.txt" "${_FILES_TO_ZIP_N}\n")
# create the zip
execute_process(COMMAND ${CMAKE_COMMAND} -E tar cvf ${PRECOMPILED_PCKG_FILE} --files-from=${DEPS_INSTALL}/files_to_zip.txt --format=zip
  WORKING_DIRECTORY "${DEPS_INSTALL}"
  RESULT_VARIABLE rv
  OUTPUT_VARIABLE ov
  ERROR_VARIABLE ev )

#check if it went well
if(NOT "${rv}" STREQUAL "0")
  message(FATAL_ERROR "could not create ${PRECOMPILED_PCKG_FILE} from ${DEPS_INSTALL}: out=${ov} err=${ev}")
endif()

message(STATUS "created ${PRECOMPILED_PCKG_FILE}")

# somehow it is a real pain to copy from DEPS_INSTALL to CMAKE_BINARY_DIR when there are subdirectories in include. Simply unpack the zip
# with cmake 3.18 we can do ... file(ARCHIVE_EXTRACT INPUT ${PRECOMPILED_PCKG_FILE} DESTINATION ${CMAKE_BINARY_DIR})
execute_process(COMMAND "${CMAKE_COMMAND}" -E tar xzf "${PRECOMPILED_PCKG_FILE}"
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
  OUTPUT_QUIET
  RESULT_VARIABLE rv )

if(NOT "${rv}" STREQUAL "0")
  message(FATAL_ERROR "could not upack ${PRECOMPILED_PCKG_FILE} to ${CMAKE_BINARY_DIR}: out=${ov} err=${ev}")
endif()

# file(GLOB_RECURSE CHECK "${CMAKE_BINARY_DIR}/lib/*")
# message("content of ${CMAKE_BINARY_DIR}/lib/* is ${CHECK}")


    
