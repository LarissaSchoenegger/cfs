import numpy as np
from numpy.linalg import norm
from numpy import sqrt
import pickle
import argparse
try:
  from matplotlib import pyplot as plt
except:
  print("failed to import matplotlib in mma.py, hopefully we don't need it.")
import sys
import os

class Global:
  def __init__(self):
    self.n = -1
    self.m = -1
    self.xl = None
    self.xu = None
    self.maxiter = -1
    self.options = { 1: 'dummy'}

  def project(self,x):
    assert len(x) == len(self.xl) == len(self.xu)
    return np.maximum(np.minimum(x,self.xu),self.xl)

glob = Global()

# convert a numpy array to valid python representation (copy from output and paste to ipython)
def tostring(nparray):
  return str(np.array(nparray).tolist()).replace('\n', '')

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("pickle", help='pickle file created by <option key="save_pickle" value="mma_initial.pickle"/>')
  args = parser.parse_args()
  dat = open(args.pickle,"rb")
  glob.xl, glob.xu, gl, gu, x_k, f_k, df_k = pickle.loads(dat.read())
  glob.n = len(glob.xl)
  glob.m = len(df_k)-1
  glob.maxiter = 1
    
  class FakeCFS:
    def __init__(self, xl, xu, gl, gu, x_k, f_k, df_k):
      self.xl = xl
      self.xu = xu
      self.gl = gl
      self.gu = gu
      self.x_k = x_k
      self.f_k = f_k
      self.df_k = df_k

    def bounds(self,xl,xu,gl,gu):
      xl[:] = self.xl[:]
      xu[:] = self.xu[:]
      gl[:] = self.gl[:]
      gu[:] = self.gu[:]
      
    def initialdesign(self,x_k):
      x_k[:] = self.x_k[:]
      print('FakeCFS:intialdesign')  
      
    def evalobj(self,x_k):
      return self.f_k[0]
      print('FakeCFS:evalobj')  

    def evalconstrs(self,x_k,g):
      g[:] = self.f_k[1:]
      print('FakeCFS:evalconstrs')  

    def evalgradobj(self,x_k,dc):
      dc[:] = self.df_k[0,:]
      print('FakeCFS:evalgradobj')  

    def evalgradconstrs(self,x_k,gc):
      n = glob.n
      m = glob.m
      for i in range(m):
        gc[i*n:(i+1)*n] = self.df_k[i+1,:] # first in df_k is objective
      
      print('FakeCFS:evalgradconstrs')  
      
    def commitIteration(self):  
      print('FakeCFS:commitIteration')
      
    def dostop(self):  
      print('FakeCFS:dostop')
      
  print(gl,gu)    
  cfs = FakeCFS(glob.xl,glob.xu,gl,gu, x_k, f_k, df_k)   
  
else:
  import cfs # module generated by openCFS


## original MMA following Svanverg, The Method of Moving Asymptotes; 1987
class Approximation:
  # n = #variables, m# inequality constraints, inequality bounds
  def __init__(self,n,m,bound):
    self.n = n
    self.m = m # note that we have the objective as m=0, hence we have m+1 functions
    assert len(bound) == m
    self.bound = bound # upper boud of inequality constraints
    
    # x, f and df exists at iteration point _k and current, therefore we add here _k to
    # make the difference clear. L,U,r, p, q are also _k but we skip this notations      
    self.x_k = np.ones(n) # design at iteration k
 
    # alpha and beta are move limits bounded by global.xl and .xu. (8) in original Svanberg
    self.alpha = np.zeros(n) # lower
    self.beta = np.zeros(n)  # upper
 
 
    self.f_k = np.zeros(m+1) # function values
    self.df_k = np.zeros((m+1,n)) # Jacobian of objective and constraints

    # common asymptotes for all functions
    self.L = np.ones(n) * 0
    self.U = np.ones(n) * 1e5
    
    self.r = np.zeros(m+1) # (5)
    
    self.p = np.zeros((m+1,n)) # 0 is objective
    self.q = np.zeros((m+1,n))
    
    # for the cfs interface
    self.g  = np.zeros(m) # constraint values
    self.dc = np.zeros(n) # cost gradient
    self.dg = np.zeros(m * n) # assume dense gradients
 
  # process function values and gradients set by set_from_cfs()
  # calculates p, q, r, alpha and beta. Needs L and U set 
  def process(self):
    assert len(self.f_k) == self.m+1
    assert self.df_k.shape == (self.m+1,self.n)
    x_k = self.x_k
    df_k = self.df_k
    
    # the move limit is optional in options
    ml = float(glob.options['move_limit']) if 'move_limit' in glob.options else np.inf
    # the relative move limit is normed by upper-lower bound
    mlr = float(glob.options['move_limit_rel']) if 'move_limit_rel' in glob.options else np.inf
    mlr *= glob.xu - glob.xl 
    assert ml > 0 and mlr.all() > 0
    # alpha and beta are the bounds for the design within an iteration to make sure we do not divide by 0 when
    # the asymptotes are within the global bounds. (8) in original Svanberg
    # reduce allows to apply maximum to more than two arguments.
    self.alpha = np.maximum.reduce([.9 * self.L + .1 * x_k, x_k - ml, x_k - mlr, glob.xl]) # alpha = L + .1 * (x-L) 
    self.beta  = np.minimum.reduce([.9 * self.U + .1 * x_k, x_k + ml, x_k + mlr, glob.xu]) # beta  = U - .1 * (U-x)
    
    dU = self.U-x_k
    dL = x_k-self.L
    #print('process',x_k,dL)
    # readability before vectorization
    for i in range(self.m+1):
      for j in range(self.n):
        self.p[i,j] = 0 if df_k[i,j] <= 0 else  dU[j]**2 * df_k[i,j] # (3)
        self.q[i,j] = 0 if df_k[i,j] >= 0 else -dL[j]**2 * df_k[i,j] # (4)
        assert self.p[i,j] >= 0 and self.q[i,j] >= 0
      self.r[i] = self.f_k[i] - np.sum((self.p[i,:] / dU) + (self.q[i,:] / dL)) # (5) note that we have x_k, not x  
       
      #print('process i',i,'f_k',self.f_k[i], 'r',self.r[i],'p',self.p[i,:],'q',self.q[i,:])
    
  # set self.f_k and self.df_k from cfs based on self.x_k
  # you need to call set() next
  def set_from_cfs(self):
    n = glob.n
    m = glob.m

    c = cfs.evalobj(self.x_k)
    cfs.evalconstrs(self.x_k, self.g)
    assert len(self.f_k) == len(self.g) +1
    self.f_k[0] = c
    self.f_k[1:] = self.g
    
    cfs.evalgradobj(self.x_k,self.dc)
    
    cfs.evalgradconstrs(self.x_k, self.dg)
    self.df_k[0,:] = self.dc
    for i in range(m):
      self.df_k[i+1,:] = self.dg[i*n:(i+1)*n]

    #print('set_from_cfs',self.x_k,'dc',delf.dc,'dg',)
    return c, self.g 
  
  # evaluate the approximation at current x in contrast to self.x_k
  # return vector with all m+1 function values
  def eval(self,x):
    f = np.zeros(self.m+1) 
    for i in range(self.m+1):
      f[i] = self.r[i] + np.sum((self.p[i] / (self.U-x)) + (self.q[i] / (x-self.L))) # (2) note that we have x, not x_k
    return f 


  def kkt(self,gu, lmbda):
    n = glob.n
    m = glob.m
    x = self.x_k
   
    #print(lmbda,gu,self.g,self.g - gu)
    print(norm(self.dc), norm(self.dg),lmbda)
   
    #dL = np.copy(self.dc)
    #for i in range(m):
      # dL += lmbda[i] * self.dg[i*n:(i+1)*n]
    dL = np.zeros(n)  
    for j in range(n):
      if x[j] > self.alpha[j] and x[j] < self.beta[j]:
        dL[j] += self.dc[j]
        for i in range(m):
          dL[j] += lmbda[i] * self.dg[i*n+j]
          
    opt = norm(dL)
    
    feas = norm(np.maximum(np.zeros(m),self.g - gu))

    return opt, feas 
    


# Dual Problem in the original version Svanverg, The Method of Moving Asymptotes; 1987
class Dual:
  # @primal a primal Appoximation with n, m, bound and eval
  def __init__(self, primal):
    self.primal = primal

  # primal variables as funktion of dual variables (17),(18),(19)
  # no need to project
  def x(self,y):  
    primal = self.primal
    n = primal.n
    m = primal.m
    p = primal.p
    q = primal.q
    L = primal.L
    U = primal.U
    alpha = primal.alpha
    beta  = primal.beta
    assert len(y) == m
    assert len(L) == len(U) == n
    assert (L < alpha).all()
    assert (beta < U).all() 
    x = np.zeros(n)
    for j in range(n):
      yp = p[0,j] + p[1:,j] @ y # p_0j + y^t p_j (15)
      yq = q[0,j] + q[1:,j] @ y
      if yp/(U[j]-alpha[j])**2 - yq/(alpha[j]-L[j])**2 >= 0: # (17)
        x[j] = alpha[j]
      elif yp/(U[j]-beta[j])**2 - yq/(beta[j]-L[j])**2 <= 0: # (18)
        x[j] = beta[j]
      else:    
        x[j] = (sqrt(yp)*L[j] + sqrt(yq)*U[j]) / (sqrt(yp) + sqrt(yq)) # (19)      
    return x

  # does not project, use glob.project()
  def x_noproject(self,y):  
    primal = self.primal
    n = primal.n
    m = primal.m
    p = primal.p
    q = primal.q
    L = primal.L
    U = primal.U
    alpha = primal.alpha
    beta =  primal.beta
    assert len(y) == m
    assert len(L) == len(U) == n
    assert (L < alpha).all()
    assert (beta < U).all() 
    x = np.zeros(n)
    for j in range(n):
      yp = p[0,j] + p[1:,j] @ y # p_0j + y^t p_j (15)
      yq = q[0,j] + q[1:,j] @ y
      x[j] = (sqrt(yp)*L[j] + sqrt(yq)*U[j]) / (sqrt(yp) + sqrt(yq)) # (19)      
      #print('x_noproject j',j,p[0,j],p[1:,j],q[0,j],q[1:,j],yp,yq,(sqrt(yp)*L[j] + sqrt(yq)*U[j]),(sqrt(yp)*L[j] + sqrt(yq)*U[j]),x[j])
    return x

  
  # dual objective function (20) and multiply with -1 for maximization
  def eval(self, y):
    primal = self.primal
    L = primal.L
    U = primal.U
    p = primal.p
    q = primal.q
    assert len(y) == primal.m
    assert len(primal.bound) == primal.m
    assert len(primal.r) == primal.m+1 # includes primal objective 
    b = primal.bound - primal.r[1:] # hard to find: third line in section 4 
 
    x = self.x(y)
    assert((x >= primal.alpha).all() and (x <= primal.beta).all())
    
    assert (L < x).all()
    assert (x < U).all()
 
    # skip vektorization
    t = 0
    for j in range(primal.n):
      t += (p[0,j] + p[1:,j] @ y) / (U[j] - x[j])
      t += (q[0,j] + q[1:,j] @ y) / (x[j] - L[j])
    # print('dual eval',y,'r0',primal.r[0],'b',b,'t',t,'x',x)
    return -1.*(primal.r[0] - (b @ y) + t) # (20) for maximization

  # this is a eval woven with x(y)
  def eval_fast(self, y):
    primal = self.primal
    n = primal.n
    m = primal.m
    p = primal.p
    q = primal.q
    L = primal.L
    U = primal.U
    alpha = primal.alpha
    beta  = primal.beta
    assert len(y) == m
    assert len(L) == len(U) == n
    assert (L < alpha).all()
    assert (beta < U).all() 
    x = np.zeros(n)
    x_j = None
    eval_t = 0
    for j in range(n):
      yp = p[0,j] + p[1:,j] @ y # p_0j + y^t p_j (15)
      yq = q[0,j] + q[1:,j] @ y
      L_j = L[j]
      U_j = U[j]
      if yp/(U_j-alpha[j])**2 - yq/(alpha[j]-L_j)**2 >= 0: # (17)  in Svanberg
        x_j = alpha[j]
      elif yp/(U_j-beta[j])**2 - yq/(beta[j]-L_j)**2 <= 0: # (18)  in Svanberg
        x_j = beta[j]
      else:    
        x_j = (sqrt(yp)*L_j + sqrt(yq)*U_j) / (sqrt(yp) + sqrt(yq)) # (19)  in Svanberg
        
      eval_t += yp / (U_j - x_j) + yq / (x_j - L_j)   # part of (20) in Svanberg

      x[j] = x_j

    # function value of dual function 
    b = primal.bound - primal.r[1:] # hard to find: third line in section 4 
    func = primal.r[0] - (b @ y) + eval_t # (20) in Svanberg
    
    return -1*func # -1 for maximization

  # gradient of dual objective (21)
  def grad(self, y): 
    
    b = self.primal.bound - self.primal.r[1:] # see eval()
    #print('dual grad',y,b, self.x(y),glob.project(self.x(y)))
    p = self.primal.p
    q = self.primal.q
    L = self.primal.L
    U = self.primal.U
    x = self.x(y)
    res = np.zeros(self.primal.m) 
    for i in range(self.primal.m):
      assert len(p[i+1,:]) == len(U) == len(x) == len(q[i+1,:]) == len(L)
      res[i] = -b[i] + np.sum(p[i+1,:] / (U - x) + q[i+1,:] / (x-L)) # (21)
    #print('dual grad',y,-1*res)  
       
    return -1.*res # for maximization
   
  # based on Aage, Lazarov, "Parallel framework for topology optimization using the method of moving asymptotes", 2011
  def hess(self,y):
    primal = self.primal
    n = primal.n
    m = primal.m
    p = primal.p
    q = primal.q
    L = primal.L
    U = primal.U
     
    x = self.x(y)
    #xp = self.x_noproject(y)
    Ux2 = (U-x)**2
    Ux3 = Ux2*(U-x)
    xL2 = (x-L)**2
    xL3 = xL2*(x-L)

    dh = np.zeros((m,n)) # d_h_ij / d_x 
    for i in range(m):
      dh[i,:] = p[i+1,:]/Ux2 - q[i+1,:]/xL2 # in the paper, after (15) is a mistake (twice p_ij)
       
    # dh is mxn, so we also need to be a matrix to end up in H is m x m   
    # dL[j,0] = 2*(p[0,j] + p[1:,j] @ y) / Ux3[j] + 2*(q[0,j] + q[1:,j] @ y) / xL3[j] 
    dLi = np.zeros((n,1))   # (d_L_jj / (d_x)**2)^-1 
    for j in range(n):
      if x[j] > primal.alpha[j] and x[j] < primal.beta[j]:
        dLi[j,0] = 1/(2*(p[0,j] + p[1:,j] @ y) / Ux3[j] + 2*(q[0,j] + q[1:,j] @ y) / xL3[j])
    
    t = dLi * dh.transpose()
    H = -1 * dh @ t  # H = -dh (dL)^1 dh^T (13)
    assert H.shape == (m,m)  
    print('dh',tostring(dh))
    print('dLi',tostring(dLi))
    print('t',tostring(t))
    print('H',tostring(H))
    return -1*H # -1 for maximization

  # optimized implementation of detmining gradient and hessian
  def func_grad_hess(self,y):
    # do x(y), grad and hess in one loop
    primal = self.primal
    n = primal.n
    m = primal.m
    p = primal.p
    q = primal.q
    L = primal.L
    U = primal.U
    alpha = primal.alpha
    beta  = primal.beta
    assert len(y) == m
    assert len(L) == len(U) == n
    assert (L < alpha).all()
    assert (beta < U).all() 
    x = np.zeros(n)
    dLi = np.zeros((n,1))   # (d_L_jj / (d_x)**2)^-1 in Aage, Lazarov
    eval_t = 0
    x_j = None
    for j in range(n):
      # Svanberg paper 
      yp = p[0,j] + p[1:,j] @ y # p_0j + y^t p_j (15)  in Svanberg
      yq = q[0,j] + q[1:,j] @ y
      L_j = L[j]
      U_j = U[j]
      if yp/(U_j-alpha[j])**2 - yq/(alpha[j]-L_j)**2 >= 0: # (17)  in Svanberg
        x_j = alpha[j]
        # dLi stays zero, see also Duysxinc DCAMM lecture
      elif yp/(U_j-beta[j])**2 - yq/(beta[j]-L_j)**2 <= 0: # (18)  in Svanberg
        x_j = beta[j]
      else:    
        x_j = (sqrt(yp)*L_j + sqrt(yq)*U_j) / (sqrt(yp) + sqrt(yq)) # (19)  in Svanberg
        assert x_j >= alpha[j] and x_j <= beta[j]  
        dLi[j,0] = 1 / (2*yp / (U_j-x_j)**3 + 2*yq / (x_j - L_j)**3) # (d_L_jj / (d_x)**2)^-1 in Aage, Lazarov
        
      eval_t += yp / (U_j - x_j) + yq / (x_j - L_j)   # part of (20) in Svanberg

      x[j] = x_j

    # function value of dual function 
    b = primal.bound - primal.r[1:] # hard to find: third line in section 4 
    func = primal.r[0] - (b @ y) + eval_t # (20) in Svanberg

    # gradient of dual function (grad) and helper (dh) for Hessian
    Ux = (U-x)
    Ux2 = Ux**2
    xL  = (x-L)
    xL2 = xL**2
    grad = np.zeros(m)  # (21) in Svanberg
    dh = np.zeros((m,n)) # d_h_ij / d_x in Aage, Lazarov
    for i in range(m):
      assert len(p[i+1,:]) == len(U) == len(x) == len(q[i+1,:]) == len(L)
      pm = p[i+1,:]
      qm = q[i+1,:]
      grad[i] = -b[i] + np.sum(pm / Ux + qm / xL) # (21) in Svanberg
      dh[i,:] = pm/Ux2 - qm/xL2 # in the paper, after (15) is a mistake (twice p_ij)
       
    t = dLi * dh.transpose()
    H = -1 * dh @ t  # H = -dh (dL)^1 dh^T (13) in Aage, Lazarov
    assert H.shape == (m,m)  
    H *= -1 # -1 for maximization
    #self.correct_hessian_trace(H)
    return -1*func, -1*grad, H # -1 for maximization

  ## correct Hessian trace as in petsc toptop and our C++ mma
  def correct_hessian_trace(self,H): 
    corr = 1.0e-4 * H.trace() / len(H)
    if -1.0 * corr < 1.0e-7:
      corr = -1.0e-7
    for i in range(len(H)):
      H[i,i] += corr 

  # plot the dual function and add sample points from history.
  # for m > 1 plot the last two y  
  def plot(self,hist):
    m = self.primal.m
    if m >= 2:
      # history sample points
      xh = [x[0][m-2] for x in hist] # before last lambda
      yh = [x[0][m-1] for x in hist] # last lambda
      vh = [x[1] for x in hist]
      
      plt.plot(vh)
      plt.show()
      
      # mesh plot 
      # https://matplotlib.org/stable/gallery/mplot3d/surface3d.html or plotviz.py
      res = 30
      xm = np.linspace(0, 1.2*max(xh) if len(xh) > 0 else 100,res) 
      ym = np.linspace(0, 1.2*max(yh) if len(yh) > 0 else 100,res)
      x, y = np.meshgrid(xm,ym)
      z = np.zeros((res,res))
      for i in range(res):
        for j in range(res):
          z[j,i] = self.eval_fast([xm[i],xm[i],ym[j]]) # currently for m=3 only
      
      ax = plt.axes(projection="3d")
      ax.plot_wireframe(x, y, z) 
      ax.scatter(xh,yh,vh,marker='o',color='red')
      plt.show()
      #sys.exit()
      
      
    else:
      xh = [x[0][0] for x in hist] 
      vh = [x[1] for x in hist]
      res = 100
      x = np.linspace(0,1.2*max(xh) if len(xh) > 0 else 100,res)
      y = [self.eval_fast(np.ones(m)*t) for t in x]
      plt.plot(x,y)
      plt.plot(xh,vh,'bo',color='red')
      plt.show()
    
       
# simple optimizers, generally based on
# Kelley, Iterative Methods for Optimization, 1999
class Optimizer:
  # nothing to do init here
  def __init__(self):
    pass    
  
  # Fletcher-Reeves (3.23)
  def beta(self,ndf, ndf_last):
    return (ndf*ndf) / (ndf_last * ndf_last)

  # simple projection
  def project(self,x,low,up):
    return np.maximum(np.minimum(x,up),low)

  # this is not the real Armijo, as we have not the scalar product bound
  # return new x 
  def linesearch(self,x, d, f):
    tau = .5
    alpha = 1
    x_org = x
    x = x + alpha * d
    #print(x_org,alpha,d,x,f(x_org),f(x))
    while f(x) >= f(x_org) - alpha and alpha > 1e-8:
      alpha *= tau
      x = x_org + alpha * d
      #print(x_org,alpha,d,x,f(x_org),f(x))
    #print(x, alpha,f(x))  
    return x

  # Simple nonlinear Conjugate Gradient Solver, Section 3.2.4 in Kelley
  # @param grad and eval are functions. No eval, no line search (but 1)
  # @param tau_r and tau_a are relative and absolute residuals
  # @return returns optimal x  
  def nlcg(self,x0,grad,func=None, tau_r=1e-3, tau_a=1e-6):
    # alg 3.2.2 nlcg
    df = grad(x0)
    r0 = norm(df)
    ndf = r0
    ndf_last = None
    x = x0
    k = 0
    while ndf > tau_r * r0 + tau_a:
      #print(ndf,x,df,func(x))
      p = -1.0*df if k == 0 else  -1*df + self.beta(ndf, ndf_last) * p # (3.23)
      assert len(p) == len(x0)
      x = self.linesearch(x,p, func) if func != None else x + p  
      ndf_last = ndf
      df = grad(x)
      ndf = norm(df)
      k += 1
    return x

  # Simple steepest descent with projection at bounds
  # algorithm 5.4.1, based on matlab implementation
  # arguements are rearanged (e.g. low before up)
  def gradproj(self,x0,grad,func,tol=1e-6,maxit=1000,init_lmbda=100,steplength=10):
    low = np.zeros(len(x0))
    up = np.ones(len(x0)) * 1e80
    assert len(x0) == len(low) == len(up)
    assert (low < up).all()
    assert (up > low).all() 
    
    if norm(x0 - self.project(x0, low, up)) > 0:
      print('gradproj: initial design not feasible')
      x0 = self.project(x0, low, up)
    alpha = 1e-4
    itc = 0
    hist = []
    fc = func(x0)
    hist.append((x0,fc))
    avg_step = 0
    avg_ls_incs = 0
    gc = grad(x0)
    xt = self.project(x0-gc, low, up)
    pgc = x0 - xt
    xc = x0 
    # ia = np.sum([1 if x0[i] <= low[i] or x[i] >= up[i] else 0 for i in range(len(a))])
    #print('initial pgc',norm(pgc),'x0',x0,'xt',xt,'gc',gc,'x0-gc',x0-gc,'low',low,'up',up)
    #print('1:iter 2:norm(pgc) 3:x')
    while norm(pgc) > tol and itc <= maxit:
       step = steplength # 1 for H
       xt = self.project(xc - step*gc, low, up)
       #print('gradproj','x0',x0,'pcg',pgc,'gc',gc,'xt',xt,xc - lmbda*gc,xt)
       ft = func(xt)    
       hist.append((xt,ft))      
       pl = xc - xt
       fgoal = fc - (pl @ pl) * (alpha/step)
       #print('gradproj pgc',pgc,'xc',xc,'x0',x0,'gc',gc,'xt',xt)
       #print(itc,norm(pgc),xc[0])
       ls_inc = 0
       while ft > fgoal:
         step *= .5
         xt = self.project(xc - step*gc, low, up)
         pl = xc - xt
         ft = func(xt)
         fgoal=fc-(pl @ pl)*(alpha/step);
         ls_inc += 1
         #print('lmbda',lmbda, 'ft',ft, 'fgoal',fgoal)
       xc = xt
       fc = func(xc)
       hist.append((xc,fc))
       gc = grad(xc)
       pgc = xc - self.project(xc - gc, low, up)           
       itc +=1
       avg_step += step
       avg_ls_incs += ls_inc
       print('iter',itc,'norm', norm(pgc),'step',step,'ls_inc',ls_inc,'xc',xc)
       
    print('gradproj pgc',norm(pgc),'iter',itc,'xc',xc,flush=True)   

    return xc,hist, avg_step/itc if itc > 0 else steplength

  def print_minor(self,minor, pgc_norm, xnew, dx,dc, do_hess, ls_steps, step, msg = None, canceled_dc = None):
    if not 'verbose_dual_variables' in glob.options:
      xnew = '{0:.3E}'.format(norm(xnew))
      dc   = '{0:.3E}'.format(norm(dc))
      canceled_dc = '{0:.3E}'.format(norm(canceled_dc)) if canceled_dc is not None else ''
    print('minor',minor,'pgc','{0:.3E}'.format(pgc_norm),'lambda',tostring(xnew),'delta','{0:.3E}'.format(dx),'dir',tostring(dc),'hess',do_hess,'ls_steps',ls_steps,'step','{0:.1E}'.format(step), end='')
    if msg:
      print('',msg,canceled_dc)
    else:
      print()

  # Simple steepest descent for Hessian steps with projection at bounds
  # algorithm 5.4.1, based on matlab implementation
  # This is not a projected Hessian w.r.t. non-active constraints!
  def grad_hess(self,x0,dual,tol=1e-5,maxit=300):
    low = np.zeros(len(x0)) 
    up = np.ones(len(x0)) * 1e80
    assert len(x0) == len(low) == len(up)
    assert (low < up).all()
    assert (up > low).all() 
    assert norm(x0 - self.project(x0, low, up)) == 0

    alpha = 1e-4
    iter = 1
    #test = dual.eval(x0)
    #print('test',test)
    
    #dual.hess(x0)
    
    fc, gc, H = dual.func_grad_hess(x0)
    ev = np.linalg.eigvals(H)
    do_hess = (ev > 1e-8).all()
    #print('primal',dual.x(x0))
    #print('r',dual.primal.r)
    #print('x0',x0,'fc',fc,'gc',gc,'H=' + tostring(H),'ev',ev)
    
    dc = np.linalg.solve(H,gc) if do_hess else gc
    #print('dh',do_hess,'dc',dc) 
    hist = []
    hist.append((x0,fc))
    xc = x0    
    xt = self.project(xc - dc, low, up)
    msg = None
    canceled_dc = None

    if do_hess and (xt <= low).any():
      msg='initial Hessian bounds lower bound'
      canceled_dc = dc
      do_hess = False
      dc = gc
    pgc = xc - xt
    
    while norm(pgc) > tol and iter <= maxit:
       step = 1 

       xt = self.project(xc - step*dc, low, up)
       ft = dual.eval_fast(xt)
       hist.append((xt,ft))
       #print('major',iter,'xc',xc,'dc',dc,'xt',xt,'ft',ft,'grad',dual.grad(xt),'hess',do_hess)
       fgoal = fc - np.sum((xc - xt)**2) * (alpha/step)
       fgoal_checkpoint = fgoal # in case we cancel a non-trustworthy Hessian
       # we need the line search to prevent a non-singular Hessian. Try with SIMP vol=.5 starting from 1
       # there occured a pathological case where ft > fgoal by 1e-12, just relax it
       ls_steps = 0
       # print(xt,(xt == 0).all(),(xt == 0).any(),ft - (fgoal + tol/100))
       #print('minor',iter,'pgc', norm(pgc) ,'xt',xt, 's',step,'xc',xc,'dc',dc,'ft',ft,'fgoal',fgoal, ' need_ls', ft > fgoal + tol/100) 
    
       while ft > fgoal + tol/100:
         #print(xt,(xt == 0).all(),(xt == 0).any())
         # do we still trust the Hessian?
         if do_hess and step < .1:
           msg = 'cancel non-trustworthy Hessian dir'
           canceled_dc = dc
           dc = gc # take gradient step
           step = 1
           xt = self.project(xc - step*dc, low, up)
           ft = dual.eval_fast(xt)
           fgoal = fgoal_checkpoint
           do_hess = False  
         else: 
           step *= .1
           xt = self.project(xc - step*dc, low, up)
           ft = dual.eval_fast(xt)
           hist.append((xt,ft))
           fgoal = fc - np.sum((xc - xt)**2) * (alpha/step)
           ls_steps += 1
         #print('step', step, 'ft',ft, 'fgoal',fgoal,'xt',xt,'dh',do_hess)

       # we do the msg when we have the new pgc
       old_dc = dc
       old_do_hess = do_hess       
       
       fc, gc, H = dual.func_grad_hess(xt)
       ev = np.linalg.eigvals(H)
       do_hess = (ev > 1e-8).all()
       dc = np.linalg.solve(H,gc) if do_hess else gc       
       hist.append((xt,fc))
              
       pgc = xt - self.project(xt - dc, low, up)           
       
       #print('after ls: pgc', norm(pgc) ,'xt',xt, 's',step,'dh',do_hess,'dc',dc,'gc',gc)
       
       self.print_minor(iter,norm(pgc),xt, norm(xc-xt),old_dc,old_do_hess,ls_steps,step,msg,canceled_dc)
       msg = None
       canceled_dc = None


       last_final_step = step
       xc = xt
       iter +=1

    # we arrive here after the loop or when we didn't enter it
    if iter <= 1:
      self.print_minor(iter,norm(pgc),xc,norm(x0-xc),dc,do_hess,0,1,msg,canceled_dc)
    
    #print('minor',iter,'pgc','{0:.3E}'.format(norm(pgc)),'xnew',xc,'dc',dc,'hess',do_hess,'ev',ev,flush=True)
    
    if iter >= maxit:
      print('cannot solve subproblem in',iter,'iterations: || x_k - x_k-1 ||',pgc)   
   
    return xc, hist
  


  def test(self):
    def func(x): 
      return x[0]**2 + 5 * x[1]**2 
   
    def grad(x): 
      return np.array([2*x[0],2*5*x[1]])   
   
    xs = opt.gradproj([1,1],grad,func,[1,1])
    #print('x^*',xs)    
   
# optional for cfs to call. 
# @return an integer with 0 for ok
def setup():
  print('mma.py: setup() called')
  return 1

# called by cfs
def init(n,m,maxiter, sim_name, options):
  
  glob.n = n
  glob.m = m
  glob.maxiter = maxiter
  glob.options = options
  
  print('mma.py: init() called n=',n, 'm=',m, 'max_iter=',maxiter, 'options=', options)
  
# expected and called by cfs
# @return an integer with 0 for ok
def solve():
  n = glob.n
  m = glob.m
  assert m > 0
  
  # bounds
  glob.xl = np.zeros(n)
  glob.xu = np.zeros(n)
  gl = np.zeros(m)
  gu = np.zeros(m)
  cfs.bounds(glob.xl,glob.xu,gl,gu)
  print('gbounds',gl, gu)
  assert (gl < gu).all()
  assert len(gl) == m

  mma = Approximation(n, m, gu)

  assert len(mma.L) == n
  mma.L = np.zeros(n)
  assert (mma.L <= glob.xl).all()
  mma.U = glob.xu * 5 # larger initial U than 1.01 make dual.x(lambda) bounded by glob.xu 

  cfs.initialdesign(mma.x_k) # read initial design from cfs
  mma.set_from_cfs()         # triggers evaluation of function values
  cfs.commitIteration()      # save values for initial design as iteration 0 in cfs
  
  #mma.U = np.ones(n) * 2 
  #mma.process()
  #x_ = np.linspace(1e-2,1,100)
  #y_ = [mma.eval(np.ones(n)*t)[1] for t in x_]
  #plt.plot(x_,y_)
  #plt.show()
  #sys.exit()

  tol = float(glob.options['sub_prob_tol']) if 'sub_prob_tol' in glob.options else 1e-5
  
  dual = Dual(mma)
  opt = Optimizer() 

  iter = 0
  
  lmbda = np.ones(m) * 15
  print('start from lmbda',lmbda,'U_min=',min(mma.U), 'U_max',max(mma.U))
  
  avg_step = 10 # for steepest descent only
  x_prev = None # x_k-1
  x_prev_prev = None # x_k-2 

  while iter < glob.maxiter and not cfs.dostop():
    if iter <= 1:
      mma.L = mma.x_k - (glob.xu - glob.xl) # (11) in Svanberg
      mma.U = mma.x_k + (glob.xu - glob.xl) # (11) in Svanberg
    else:
      assert x_prev_prev is not None
      sign = (mma.x_k - x_prev) * (x_prev - x_prev_prev)
      s = np.array([.7 if t < 0 else 1/.7 for t in sign]) 
      mma.L = mma.x_k - s * (x_prev - mma.L) # (12) in Svanberg
      mma.U = mma.x_k + s * (mma.U - x_prev) # (12) in Svanberg
    #print('x',mma.x_k)
    #print('L',mma.L)
    #print('U',mma.U) 
    #print(np.amin(mma.L),np.amax(mma.L),np.amin(mma.U),np.amax(mma.U))
    
    mma.process() # we need to go another round with the subproblem

    if 'save_pickle' in glob.options:
      o = (glob.xl, glob.xu, gl, gu, mma.x_k, mma.f_k, mma.df_k)
      st = os.path.splitext(glob.options['save_pickle']) # ('mma_initial', '.pickle') or empty second string 
      name = st[0]+'-iter_' + str(iter) + st[1]
      out = open(name,"wb")
      out.write(pickle.dumps(o))
      print('wrote pickle data to ', name)
    
    #print('L',mma.L,'U',mma.U)
    #print('alpha',mma.alpha,'beta',mma.beta)
    
    # we have a current approximation and solve the dual problem
    #lmbda,hist,avg_step = opt.gradproj(lmbda, dual.grad, dual.eval, tol=1e-4,maxit=100,steplength=avg_step*10) 
    lmbda,hist = opt.grad_hess(lmbda, dual,tol=tol, maxit=20)
    if 'plot_dual' in glob.options:
      dual.plot(hist)
    
    # next design candidate - projected
    x_prev_prev = x_prev
    x_prev = mma.x_k
    mma.x_k = dual.x(lmbda)

    # uses current mma.x_k and triggers evaluation in cfs
    c, g = mma.set_from_cfs() 
    
    #optm, feas = mma.kkt(gu, lmbda)
    #print('opt',optm,'feas',feas)
    
    
    cfs.commitIteration()
    iter += 1    
    #print('major',iter,'L',np.min(mma.L),'...',np.max(mma.L),'U',np.min(mma.U),'...',np.max(mma.U))
  
  return 0




if __name__ == '__main__':
  solve()
